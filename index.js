const base_url = "https://api.ipify.org/?format=json";
const btn = document.querySelector("button");
const root = document.querySelector("#root");

btn.addEventListener("click", findIP);

async function findIP() {
  let response = await fetch(base_url);
  let user = await response.json().then(({ ip }) => ip);
  console.log(user);

  let resultIP = await fetch(`http://ip-api.com/json/${user}`);
  let data = await resultIP.json();
  console.log(data);
  const { country, timezone, regionName, city } = data;
  const info = document.createElement("p");
  info.innerHTML = `${timezone}<br>${country}<br>${regionName}<br>${city}`;
  root.append(info);
}
